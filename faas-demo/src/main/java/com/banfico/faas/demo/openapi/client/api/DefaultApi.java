package com.banfico.faas.demo.openapi.client.api;

import com.banfico.faas.demo.openapi.client.invoker.ApiClient;

import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelAccount;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelAccounts;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelBalances;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelTransactions;
import com.banfico.faas.demo.openapi.client.model.ControllerModelProviders;
import com.banfico.faas.demo.openapi.client.model.ControllerModelProvidersWithGrantAccessForm;
import com.banfico.faas.demo.openapi.client.model.ControllerModelRedirect;
import com.banfico.faas.demo.openapi.client.model.ControllerModelSingleProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-12-03T14:47:16.599+05:30[Asia/Kolkata]")
@Component("com.banfico.faas.demo.openapi.client.api.DefaultApi")
public class DefaultApi {
    private ApiClient apiClient;

    public DefaultApi() {
        this(new ApiClient());
    }

    @Autowired
    public DefaultApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * /api/oauth2/authorization_code/code-callback
     * Callback which must be called at the end of the authorization_code flow triggered by openbanking.endpoints.providers.available.grantAccess
     * <p><b>303</b> - success
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public void oauth2AuthorizationCodeCallback() throws RestClientException {
        oauth2AuthorizationCodeCallbackWithHttpInfo();
    }

    /**
     * /api/oauth2/authorization_code/code-callback
     * Callback which must be called at the end of the authorization_code flow triggered by openbanking.endpoints.providers.available.grantAccess
     * <p><b>303</b> - success
     * @return ResponseEntity&lt;Void&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Void> oauth2AuthorizationCodeCallbackWithHttpInfo() throws RestClientException {
        Object postBody = null;
        
        String path = apiClient.expandPath("/api/oauth2/authorization_code/code-callback", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Void> returnType = new ParameterizedTypeReference<Void>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/available
     * Lists available ASPSP. Note that grantAccessForm attribute will be set if needed and if so the data of the form must be submitted to integration endpoint.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param providersIds Fintech user identifier. (required)
     * @param forms Fintech user identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorControllerModelProvidersWithGrantAccessForm
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ControllerModelProvidersWithGrantAccessForm providersAvailable(String xUserId, String xAppId, List<String> providersIds, List<String> forms) throws RestClientException {
        return providersAvailableWithHttpInfo(xUserId, xAppId, providersIds, forms).getBody();
    }

    /**
     * /api/providers/available
     * Lists available ASPSP. Note that grantAccessForm attribute will be set if needed and if so the data of the form must be submitted to integration endpoint.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param providersIds Fintech user identifier. (required)
     * @param forms Fintech user identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorControllerModelProvidersWithGrantAccessForm&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ControllerModelProvidersWithGrantAccessForm> providersAvailableWithHttpInfo(String xUserId, String xAppId, List<String> providersIds, List<String> forms) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersAvailable");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersAvailable");
        }
        
        // verify the required parameter 'providersIds' is set
        if (providersIds == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'providersIds' when calling openbankingEndpointsProvidersAvailable");
        }
        
        // verify the required parameter 'forms' is set
        if (forms == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'forms' when calling openbankingEndpointsProvidersAvailable");
        }
        
        String path = apiClient.expandPath("/api/providers/available", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        queryParams.putAll(apiClient.parameterToMultiValueMap(ApiClient.CollectionFormat.valueOf("csv".toUpperCase(Locale.ROOT)), "providersIds", providersIds));
        queryParams.putAll(apiClient.parameterToMultiValueMap(ApiClient.CollectionFormat.valueOf("csv".toUpperCase(Locale.ROOT)), "forms", forms));

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ControllerModelProvidersWithGrantAccessForm> returnType = new ParameterizedTypeReference<ControllerModelProvidersWithGrantAccessForm>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated
     * Lists integrated ASPSP with contextual x-user-id. This endpoint populates access model attribute.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorControllerModelProviders
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ControllerModelProviders providersIntegrated(String xUserId, String xAppId) throws RestClientException {
        return providersIntegratedWithHttpInfo(xUserId, xAppId).getBody();
    }

    /**
     * /api/providers/integrated
     * Lists integrated ASPSP with contextual x-user-id. This endpoint populates access model attribute.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorControllerModelProviders&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ControllerModelProviders> providersIntegratedWithHttpInfo(String xUserId, String xAppId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegrated");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegrated");
        }
        
        String path = apiClient.expandPath("/api/providers/integrated", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ControllerModelProviders> returnType = new ParameterizedTypeReference<ControllerModelProviders>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/accounts
     * Lists known accounts of the contextual x-user-id.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorApiConnectorModelAccounts
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ApiConnectorModelAccounts providersIntegratedAccounts(String xUserId, String xAppId) throws RestClientException {
        return providersIntegratedAccountsWithHttpInfo(xUserId, xAppId).getBody();
    }

    /**
     * /api/providers/integrated/accounts
     * Lists known accounts of the contextual x-user-id.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorApiConnectorModelAccounts&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ApiConnectorModelAccounts> providersIntegratedAccountsWithHttpInfo(String xUserId, String xAppId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAccounts");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAccounts");
        }
        
        String path = apiClient.expandPath("/api/providers/integrated/accounts", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ApiConnectorModelAccounts> returnType = new ParameterizedTypeReference<ApiConnectorModelAccounts>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}
     * Get information about the related ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorControllerModelSingleProvider
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ControllerModelSingleProvider providersIntegratedAspsp(String xUserId, String xAppId, String aspspId) throws RestClientException {
        return providersIntegratedAspspWithHttpInfo(xUserId, xAppId, aspspId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}
     * Get information about the related ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorControllerModelSingleProvider&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ControllerModelSingleProvider> providersIntegratedAspspWithHttpInfo(String xUserId, String xAppId, String aspspId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAspsp");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAspsp");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedAspsp");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ControllerModelSingleProvider> returnType = new ParameterizedTypeReference<ControllerModelSingleProvider>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}/accounts
     * Lists known accounts of the contextual x-user-id for a particular ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorApiConnectorModelAccounts
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ApiConnectorModelAccounts providersIntegratedAspspAccounts(String xUserId, String xAppId, String aspspId) throws RestClientException {
        return providersIntegratedAspspAccountsWithHttpInfo(xUserId, xAppId, aspspId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}/accounts
     * Lists known accounts of the contextual x-user-id for a particular ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorApiConnectorModelAccounts&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ApiConnectorModelAccounts> providersIntegratedAspspAccountsWithHttpInfo(String xUserId, String xAppId, String aspspId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAspspAccounts");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAspspAccounts");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedAspspAccounts");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}/accounts", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ApiConnectorModelAccounts> returnType = new ParameterizedTypeReference<ApiConnectorModelAccounts>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}/balances
     * Lists of balances for a particular account.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorApiConnectorModelBalances
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ApiConnectorModelBalances providersIntegratedAspspAccountsBalances(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        return providersIntegratedAspspAccountsBalancesWithHttpInfo(xUserId, xAppId, aspspId, accountId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}/balances
     * Lists of balances for a particular account.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorApiConnectorModelBalances&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ApiConnectorModelBalances> providersIntegratedAspspAccountsBalancesWithHttpInfo(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsBalances");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsBalances");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsBalances");
        }
        
        // verify the required parameter 'accountId' is set
        if (accountId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'accountId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsBalances");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        uriVariables.put("accountId", accountId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}/accounts/{accountId}/balances", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ApiConnectorModelBalances> returnType = new ParameterizedTypeReference<ApiConnectorModelBalances>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}
     * Lists known accounts of the contextual x-user-id for a particular ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorApiConnectorModelAccount
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ApiConnectorModelAccount providersIntegratedAspspAccountsId(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        return providersIntegratedAspspAccountsIdWithHttpInfo(xUserId, xAppId, aspspId, accountId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}
     * Lists known accounts of the contextual x-user-id for a particular ASPSP.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorApiConnectorModelAccount&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ApiConnectorModelAccount> providersIntegratedAspspAccountsIdWithHttpInfo(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsId");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsId");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsId");
        }
        
        // verify the required parameter 'accountId' is set
        if (accountId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'accountId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsId");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        uriVariables.put("accountId", accountId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}/accounts/{accountId}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ApiConnectorModelAccount> returnType = new ParameterizedTypeReference<ApiConnectorModelAccount>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}/transactions
     * Lists of transactions for a particular account.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ComBanficoOpenbankingOrchestratorApiConnectorModelTransactions
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ApiConnectorModelTransactions providersIntegratedAspspAccountsTransactions(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        return providersIntegratedAspspAccountsTransactionsWithHttpInfo(xUserId, xAppId, aspspId, accountId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}/accounts/{accountId}/transactions
     * Lists of transactions for a particular account.
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param accountId Account identifier. (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorApiConnectorModelTransactions&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ApiConnectorModelTransactions> providersIntegratedAspspAccountsTransactionsWithHttpInfo(String xUserId, String xAppId, String aspspId, String accountId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsTransactions");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsTransactions");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsTransactions");
        }
        
        // verify the required parameter 'accountId' is set
        if (accountId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'accountId' when calling openbankingEndpointsProvidersIntegratedAspspAccountsTransactions");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        uriVariables.put("accountId", accountId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}/accounts/{accountId}/transactions", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ApiConnectorModelTransactions> returnType = new ParameterizedTypeReference<ApiConnectorModelTransactions>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}
     * Will trigger the creation of a token for the contextual tpp/aspsp/user
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param requestBody  (required)
     * @return ComBanficoOpenbankingOrchestratorControllerModelRedirect
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ControllerModelRedirect providersIntegratedGrantAccess(String xUserId, String xAppId, String aspspId, List<Object> requestBody) throws RestClientException {
        return providersIntegratedGrantAccessWithHttpInfo(xUserId, xAppId, aspspId, requestBody).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}
     * Will trigger the creation of a token for the contextual tpp/aspsp/user
     * <p><b>200</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @param requestBody  (required)
     * @return ResponseEntity&lt;ComBanficoOpenbankingOrchestratorControllerModelRedirect&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<ControllerModelRedirect> providersIntegratedGrantAccessWithHttpInfo(String xUserId, String xAppId, String aspspId, List<Object> requestBody) throws RestClientException {
        Object postBody = requestBody;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedGrantAccess");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedGrantAccess");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedGrantAccess");
        }
        
        // verify the required parameter 'requestBody' is set
        if (requestBody == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'requestBody' when calling openbankingEndpointsProvidersIntegratedGrantAccess");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ControllerModelRedirect> returnType = new ParameterizedTypeReference<ControllerModelRedirect>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * /api/providers/integrated/{aspspId}
     * Revoke access for the configured ASPSP for contextual x-user-id.
     * <p><b>204</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return Object
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Object providersIntegratedRevokeAccess(String xUserId, String xAppId, String aspspId) throws RestClientException {
        return providersIntegratedRevokeAccessWithHttpInfo(xUserId, xAppId, aspspId).getBody();
    }

    /**
     * /api/providers/integrated/{aspspId}
     * Revoke access for the configured ASPSP for contextual x-user-id.
     * <p><b>204</b> - success
     * @param xUserId Fintech user identifier. (required)
     * @param xAppId Fintech application identifier. (required)
     * @param aspspId ASPSP identifier. (required)
     * @return ResponseEntity&lt;Object&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Object> providersIntegratedRevokeAccessWithHttpInfo(String xUserId, String xAppId, String aspspId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'xUserId' is set
        if (xUserId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xUserId' when calling openbankingEndpointsProvidersIntegratedRevokeAccess");
        }
        
        // verify the required parameter 'xAppId' is set
        if (xAppId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'xAppId' when calling openbankingEndpointsProvidersIntegratedRevokeAccess");
        }
        
        // verify the required parameter 'aspspId' is set
        if (aspspId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'aspspId' when calling openbankingEndpointsProvidersIntegratedRevokeAccess");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("aspspId", aspspId);
        String path = apiClient.expandPath("/api/providers/integrated/{aspspId}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        if (xUserId != null)
        headerParams.add("x-user-id", apiClient.parameterToString(xUserId));
        if (xAppId != null)
        headerParams.add("x-app-id", apiClient.parameterToString(xAppId));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Object> returnType = new ParameterizedTypeReference<Object>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
}
