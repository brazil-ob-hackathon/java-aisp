package com.banfico.faas.demo.service.aggregate.balance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.banfico.faas.demo.openapi.client.api.DefaultApi;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelAccounts;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelBalance;
import com.banfico.faas.demo.openapi.client.model.ApiConnectorModelBalances;
import com.banfico.faas.demo.openapi.client.model.ControllerModelProvider;

@Service
public class AggregateUserAccountBalanceService {

	@Autowired
	protected RestTemplate restTemplate;

	@Autowired
	private DefaultApi api;

	public Map<String, BigDecimal> getUserAspspBalance(String xAppId, String xUserId, String aspspId) {

		ApiConnectorModelAccounts accounts = api.providersIntegratedAspspAccounts(xUserId, xAppId, aspspId);

		final List<ApiConnectorModelBalance> balList2 = new ArrayList<>();

		getBalanceForEachAccounts(xAppId, xUserId, accounts, balList2);

		final Map<String, BigDecimal> bal = new HashMap<>();

		calulateBal(balList2, bal);
		return bal;

	}

	public Map<String, BigDecimal> getUserAspspAllBalance(String xAppId, String xUserId) {

		ApiConnectorModelAccounts accounts = api.providersIntegratedAccounts(xUserId, xAppId);

		final List<ApiConnectorModelBalance> balList2 = new ArrayList<>();

		getBalanceForEachAccounts(xAppId, xUserId, accounts, balList2);

		final Map<String, BigDecimal> bal = new HashMap<>();

		calulateBal(balList2, bal);
		return bal;
	}

	private void getBalanceForEachAccounts(String xAppId, String xUserId, ApiConnectorModelAccounts accounts,
			final List<ApiConnectorModelBalance> balList2) {
		accounts.getAccounts().forEach(ac -> {
			ControllerModelProvider p = (ControllerModelProvider) ac.getProvider();
			String acAspspId = p.getId();
			ApiConnectorModelBalances balances = api.providersIntegratedAspspAccountsBalances(xUserId, xAppId,
					acAspspId, ac.getResourceId());

			if (!CollectionUtils.isEmpty(balances.getBalances())) {
				balList2.addAll(balances.getBalances());
			}

		});
	}

	private void calulateBal(final List<ApiConnectorModelBalance> balList2, final Map<String, BigDecimal> bal) {
		balList2.forEach(b -> {
			String cur = b.getBalanceAmount().getCurrency();
			String amt = b.getBalanceAmount().getAmount();

			BigDecimal val = bal.get(cur);
			if (val == null) {
				bal.put(cur, new BigDecimal(amt));
			} else {
				bal.put(cur, val.add(new BigDecimal(amt)));
			}

		});
	}

}
