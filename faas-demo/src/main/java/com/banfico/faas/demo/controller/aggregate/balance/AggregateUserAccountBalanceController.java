package com.banfico.faas.demo.controller.aggregate.balance;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.banfico.faas.demo.service.aggregate.balance.AggregateUserAccountBalanceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "User Balances API", tags = "userBalance")
@RestController
@RequestMapping("/api/faas/demo")
public class AggregateUserAccountBalanceController {

	private static final Logger log = LogManager.getLogger(AggregateUserAccountBalanceController.class.getName());

	@Autowired
	private AggregateUserAccountBalanceService service;

	@ApiOperation(value = "Get User's aggregate balance for ASPSPs", nickname = "get")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregate/balance/{aspspId}/{x-user-id}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> get(@PathVariable("aspspId") String aspspId,
			@PathVariable("x-user-id") String xUserId, @RequestHeader("x-app-id") String xAppId) {
		log.debug("Get User's aggregate balance for ASPSP : {} , xUserId: {}", aspspId, xUserId);
		Map<String, BigDecimal> userAspspBalance = service.getUserAspspBalance(xAppId, xUserId, aspspId);
		return new ResponseEntity<>(userAspspBalance, HttpStatus.OK);
	}

	@ApiOperation(value = "Get User's aggregate balance for all", nickname = "get")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregate/balance/all/{x-user-id}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> get(@PathVariable("x-user-id") String xUserId,
			@RequestHeader("x-app-id") String xAppId) {
		log.debug("Get User's aggregate balance for xUserId: {}", xUserId);
		Map<String, BigDecimal> userAspspBalance = service.getUserAspspAllBalance(xAppId, xUserId);
		return new ResponseEntity<>(userAspspBalance, HttpStatus.OK);
	}

}
