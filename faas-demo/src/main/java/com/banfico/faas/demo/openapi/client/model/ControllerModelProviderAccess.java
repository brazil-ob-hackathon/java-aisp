/*
 * OpenBanking Orchestrator API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.banfico.faas.demo.openapi.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.openapitools.jackson.nullable.JsonNullable;
import java.util.NoSuchElementException;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * ComBanficoOpenbankingOrchestratorControllerModelProviderAccess
 */
@JsonPropertyOrder({
  ControllerModelProviderAccess.JSON_PROPERTY_CREATED,
  ControllerModelProviderAccess.JSON_PROPERTY_EXPIRES
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-12-03T14:47:16.599+05:30[Asia/Kolkata]")
public class ControllerModelProviderAccess {
  public static final String JSON_PROPERTY_CREATED = "created";
  private JsonNullable<String> created = JsonNullable.<String>undefined();

  public static final String JSON_PROPERTY_EXPIRES = "expires";
  private JsonNullable<String> expires = JsonNullable.<String>undefined();


  public ControllerModelProviderAccess created(String created) {
    this.created = JsonNullable.<String>of(created);
    
    return this;
  }

   /**
   * Get created
   * @return created
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonIgnore

  public String getCreated() {
        return created.orElse(null);
  }

  @JsonProperty(JSON_PROPERTY_CREATED)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public JsonNullable<String> getCreated_JsonNullable() {
    return created;
  }
  
  @JsonProperty(JSON_PROPERTY_CREATED)
  public void setCreated_JsonNullable(JsonNullable<String> created) {
    this.created = created;
  }

  public void setCreated(String created) {
    this.created = JsonNullable.<String>of(created);
  }


  public ControllerModelProviderAccess expires(String expires) {
    this.expires = JsonNullable.<String>of(expires);
    
    return this;
  }

   /**
   * Get expires
   * @return expires
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonIgnore

  public String getExpires() {
        return expires.orElse(null);
  }

  @JsonProperty(JSON_PROPERTY_EXPIRES)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public JsonNullable<String> getExpires_JsonNullable() {
    return expires;
  }
  
  @JsonProperty(JSON_PROPERTY_EXPIRES)
  public void setExpires_JsonNullable(JsonNullable<String> expires) {
    this.expires = expires;
  }

  public void setExpires(String expires) {
    this.expires = JsonNullable.<String>of(expires);
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ControllerModelProviderAccess comBanficoOpenbankingOrchestratorControllerModelProviderAccess = (ControllerModelProviderAccess) o;
    return Objects.equals(this.created, comBanficoOpenbankingOrchestratorControllerModelProviderAccess.created) &&
        Objects.equals(this.expires, comBanficoOpenbankingOrchestratorControllerModelProviderAccess.expires);
  }

  @Override
  public int hashCode() {
    return Objects.hash(created, expires);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ComBanficoOpenbankingOrchestratorControllerModelProviderAccess {\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    expires: ").append(toIndentedString(expires)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

