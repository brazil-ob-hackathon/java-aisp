#!/bin/sh
echo "\n* building 'faas-demo services war'...\n";
mvn clean install -DskipTests -f faas-demo/pom.xml
cp -p ./faas-demo/target/faas-demo.war ./docker/tomcat/faas-demo.war
echo "\n* building 'faas-demo services is done.\n";

echo "\n* building 'tomcat container'...\n";
docker build -t  faas-demo-api:latest ./docker/tomcat;
echo "\n* building 'tomcat container is done.\n";

echo "\n* building 'postgres db'...\n";
docker build -t faas-demo-db:latest ./docker/postgres;
echo "\n* building 'postgres db is done.\n";


